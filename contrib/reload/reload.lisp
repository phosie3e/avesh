(in-package #:avesh.reload)

(defcommandhook reload () (command arguments next)
  (when (= (length arguments) 0)
    (error "Not enough argument to reload."))
  (dolist (arg arguments)
    (load-module arg)))
