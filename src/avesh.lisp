(in-package #:avesh)

(export '*default-contrib-modules*)
(defvar *default-contrib-modules* '("cd" "history" "expansion" "alias" "pipe"))

(export '*default-contrib-folders*)
(defvar *default-contrib-folders* `(,(merge-pathnames #p ".avesh/contrib/"
                                                      (user-homedir-pathname))))

(export '*exit-message*)
(defvar *exit-message* "~%bye bye honey~~ -avesh~%")

(export '*debug*)
(defvar *debug* nil)

(let ((build-options-file #p ".build-options"))
  (when (probe-file build-options-file)
    (with-open-file (f build-options-file)
      ;; First line is $PREFIX
      (let ((prefix (read-line f)))
        (push (if prefix
                  (merge-pathnames #p "share/avesh/contrib/" (pathname prefix))
                  #p "/usr/share/avesh/contrib/")
              *default-contrib-folders*)))))

(export 'main)
(defun main ()
  (when (uiop:getenv "DEBUG")
    (setf *debug* t))
  (let ((user-file (merge-pathnames #p ".aveshrc" (user-homedir-pathname))))
    (when (probe-file user-file)
      (load user-file)))
  (map nil #'setup-modules-load-path *default-contrib-folders*)
  (map nil #'load-module *default-contrib-modules*)
  (handler-case
      (call-startup-hooks (ordered-hooks *hooks* :startup))
    (error (e) (format t "Startup hook error: ~A~%" e)))
  (loop
     (handler-case
         (let* ((line (rl:readline :prompt (funcall *prompt-callback*)
                                   :add-history t))
                (parts (parse-parts (or line "")))
                (command (first parts))
                (arguments (rest parts)))
           ;; If readline returned NIL, in practice, this is because
           ;; we hit C-d. Exit the terminal in this case.
           (when (null line)
             (format t *exit-message*)
             (uiop:quit))
           (handler-case
               (call-command-hooks
                (ordered-hooks *hooks* :command) command arguments)
             (error (e) (format t "Command hook error: ~A~%" e)))
           (force-output))
       (sb-sys:interactive-interrupt ()
         (loop for child = (pop *children*)
            until (null child)
            do (handler-case
                   (sb-posix:kill child sb-unix:sigint)
                 ;; Just silently ignore errors.
                 ;; TODO: find a better way.
                 (error () nil)))))))

;;; A copy paste from:
;;; com.informatimago.common-lisp.lisp-reader.reader:set-syntax-from-char
;;; With a minor modification.
(defun set-syntax-with-trait-from-char (to-char from-char
                                        &optional (to-readtable reader:*readtable*)
                                          (from-readtable reader::*standard-readtable*))
  "
DO:     Copy the syntax between characters in the readtable.
URL:    <http://www.lispworks.com/documentation/HyperSpec/Body/f_set_sy.htm>
"
  (let* ((frst  (reader:readtable-syntax-table from-readtable))
         (trst  (reader:readtable-syntax-table   to-readtable))
         (fcd   (reader::character-description frst from-char))
         (tcd   (reader::character-description trst   to-char)))
    (setf (reader::character-description trst to-char)
          (make-instance 'reader::character-description
                         :syntax   (reader::character-syntax fcd)
                         ;; constituent traits are copied.
                         ;; This is the only change avesh needs.
                         :traits   (reader::character-constituent-traits fcd)
                         ;; macros are copied only if from is a macro character.
                         :macro    (or (reader::character-macro fcd)
                                       (reader::character-macro tcd))
                         :dispatch (if (reader::character-dispatch fcd)
                                       (reader::copy-hash-table
                                        (reader::character-dispatch fcd))
                                       (reader::character-dispatch tcd)))))
  t)

(defun parse-parts (line)
  "Parse the parts of a command line.

Generally speaking, (read-from-string) with the correct
readtable is good enough, given that we (symbol-name)
every component. Some special cases exist though, like ..
or |, so we need to special-case them, usually by copying their
syntax type, with or without their trait."
  (handler-case
      (let ((reader:*readtable* (reader:copy-readtable nil)))
        (setf (reader:readtable-case reader:*readtable*) :preserve)
        ;; Take #\! as a constituent syntax type for the special
        ;; characters that need to work in the terminal.
        (map nil (lambda (char) (reader:set-syntax-from-char char #\!))
             '(#\| #\#))
        ;; #\* is just a normal part of a symbol, make #\: act
        ;; the same.
        (map nil (lambda (char) (set-syntax-with-trait-from-char char #\*))
             '(#\: #\.))
        ;; The single quote should be like a double quote.
        (set-syntax-with-trait-from-char #\' #\")
        (mapcar
         (lambda (part) (if (symbolp part) (symbol-name part) part))
         (reader:read-from-string
          (format nil "(~A)" line))))
    (error (e) (format t "Unable to parse line: ~A~%" e))))

(defun call-startup-hooks (hooks)
  (when hooks
    (funcall (first hooks)
             (lambda ()
               (when (rest hooks)
                 (call-startup-hooks (rest hooks)))))))

(defun call-command-hooks (hooks command arguments)
  (when hooks
    (funcall (first hooks)
             command
             arguments
             (lambda (command arguments)
               (when (rest hooks)
                 (call-command-hooks (rest hooks) command arguments))))))
