# avesh

A Very Extensible Shell.

The goal of avesh is to be super extensible. The point is to be able
to hack around the shell as much as we want.

Bash and zsh, although powerful, have fairly limited hackability. One
thing I wanted to do was being able to prevent a command from
running, and run something else instead. I couldn't. With avesh, I
can.

This is a minimal screenshot of avesh in action:

![Avesh in action](http://i.imgur.com/q8qhGB9.png)

You can see several things in this screenshot:

- A customized prompt
- A timestamp appended after every command
- A Lisp function call

The customized prompt was defined in the `~/.aveshrc`. Look at the
[example aveshrc file](example-aveshrc) to see how this prompt was
done.

The timestamp is a avesh contrib module. The module hooks before every
command is run, and shows a timestamp before running the command.

The Lisp function all is thanks to another avesh contrib module. Using
[shelly](https://github.com/fukamachi/shelly) and defining `#` as a
command, whenever you run `# foo`, it's like running `(foo)` in the
Lisp REPL. Even better: it's actually run within avesh process!

Avesh uses `readline` to manage the prompt. It means that shortcuts
such as `Ctrl-a` (beginning of line), `Ctrl-e` (end of line) or
`Ctrl-r` (history) work out of the box.

### Installation instructions

Requirements: sbcl, make.

```shell
$ git clone https://gitlab.com/ralt/avesh
$ cd avesh/
$ ./configure
$ make
$ sudo make install
```

The `configure` script has one option: `--prefix`, to choose where the
contrib folders are read by default. Without the option, the default
value is `/usr/local/`, which means it will find the contrib folders
in `/usr/local/share/avesh/contrib/`.

The `make install` command supports the `DESTDIR` variable to define
where everything should be installed. Without the variable, the
default value is `/usr/local`, which means it will install the binary
in `/usr/local/bin/avesh` and the default contrib folder in
`/usr/local/share/avesh/contrib`.

### Contrib modules

Avesh provides a list of contrib modules, and some of them are loaded
by default. These modules are:

- `cd`: provides the `cd` command.
- `history`: provides reading history in `~/.history` when avesh
  starts, writing to the same file after every command, and the
  command `history`.
- `expansion`: expands the globs such as `~` and `*`.
- `alias`: provides an `alias` command.
- `pipe`: provides piping. Both infix and prefix piping.

Infix/prefix piping is better explained with a simple example. These
all work:

```shell
$ ls | grep c
common-lisp
contrib
avesh.asd
example-aveshrc
quicklisp.lisp
src
$ | ls (grep c)
common-lisp
contrib
avesh.asd
example-aveshrc
quicklisp.lisp
src
$ | (| ls (grep c)) (grep r)
contrib
avesh.asd
example-aveshrc
src
```

Some other contrib modules are provided, and loading them is as simple
as running `(load-module "your-module")` in the `~/.aveshrc`.

- `shelly`: provides a `#` command to run shelly commands.
- `time-command`: displays a timestamp after every command.
- `reload`: reloads a contrib module.

### `~/.aveshrc`

The `~/.aveshrc` file is loaded every time avesh is started.

The first line of your aveshrc should be `(in-package #:avesh-user)`.
This package is provided to avoid conflicts with avesh itself.

Here is the definition of the package:

```lisp
(defpackage #:avesh-user
  (:use #:cl #:avesh))
```

This means that all the symbols exported by avesh will be directly
available.


### API

Avesh core is very minimal. Even commands such as `cd` are
exported. It provides an API that you can use in your `~/.aveshrc` to
hack around whatever you need to.

#### Prompt

To decide what the prompt looks like, avesh will call the function
stored in `avesh:*prompt-callback*`.

An example

```lisp
(setf *prompt-callback*
      (lambda ()
        (format nil "~A$ " (uiop:getcwd))))
```

Will show a prompt like this:

```shell
/home/foo/bar/$
```

#### Hooks

Avesh provides a way to define "hooks". Hooks are called at different
times in avesh's life, and they're designed to play together nicely.

There are 2 types of hooks: startup and command.

The startup hooks are run once when avesh starts. An example of such a
hook is to load the history.

The command hooks are run every time the user presses `Enter` in the
shell.

Here is an example of such a hook:

```lisp
(defhook example (:weight 10 :type :command) (command arguments next)
  (format t "Hello, world!~%")
  (funcall next command arguments))
```

First off, the `:weight` and `:command` options are optional. The
default values are respectively `0` and `:command`.

The weight defines the order of the hook among all the hooks. The type
is the type of hook (`:command` or `:startup`).

The 3 arguments passed to the hook are:

- `command`: a string with the command that was run. This is the 1st
  item in the command line.
- `arguments`: a list of the arguments passed in the command
  line. Does not include the command.
- `next`: a function that will call the next hook.

The `next` function is unusual. This means that every time your hook
is not concerned by the command that was run, it should call this
function for the next hook to be called. This is what lets avesh be
very extensible: the hooks can decide whether they want to stop
everything or not by simply calling or not the next function.

To provide a smoother experience for commands, another macro is
available:

```lisp
(defcommandhook example () (command arguments next)
  (format t "foo~%"))
```

Which expands to something equivalent to this:

```lisp
(defhook example () (command arguments next)
  (unless (string= command "example")
    (return-from example (funcall next command arguments)))
  (format t "foo~%"))
```

#### Modules

To load a module, you simply need `(load-module "your-module")`.

The contrib modules must be in either:

- `/usr/share/avesh/contrib/`
- `~/.avesh/contrib/`

Contrib modules are plain Lisp systems. You can load them in any way
you like, but avesh provides some sugar around this.

For this sugar to work, the modules must follow some convention:

- Every module must be in a folder inside the `contrib/` folder.
- The name of the system must be `avesh.<module name>`.
- Hence, the name of the asd file must be `avesh.<module name>.asd`.

Avesh will load the modules by using quicklisp, so you can put
dependencies in your system, and they will be automatically
downloaded.

The list of default modules is stored in the variable
`*default-contrib-modules*`, and can be changed like any list.
